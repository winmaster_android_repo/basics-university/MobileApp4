package katarzyna.mobileapp4;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity
{

    private FileOutputStream os;
    private FileInputStream is;
    private TextView napisPowitalny;
    private SharedPreferences pref;
    private String tekstPowitalny;
    private View mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        napisPowitalny = (TextView)findViewById(R.id.textPowitalny);
        mainView = findViewById(R.id.activity_main);
        pref = getSharedPreferences("plik", Activity.MODE_PRIVATE);
        checkFile();
        getData();
    }

    protected void onRestart()
    {
        super.onRestart();
        getData();
    }

    public void addData(View view)
    {
        Intent add = new Intent(this,ChangeActivity.class);
        startActivity(add);
    }

    public void goToList(View view)
    {
        Intent ToList = new Intent(this, katarzyna.mobileapp4.ListActivity.class);
        startActivity(ToList);
    }

    public void goToData(View view)
    {
        Intent toData = new Intent(this,AddDataActivity.class);
        startActivity(toData);
    }

    private void getData()
    {
        tekstPowitalny = pref.getString("napis","Brak napisu");
        napisPowitalny.setText(tekstPowitalny);
        mainView.setBackgroundColor(pref.getInt("tlo", Color.DKGRAY));
        napisPowitalny.setTextColor(pref.getInt("tColor",Color.BLACK));
        napisPowitalny.setTextSize(pref.getInt("wielkosc",25));
    }

    private void checkFile()
    {

        byte[] byff = null;
        try
        {
            is = openFileInput("daneApp");
            byff = new byte[1024];
            is.read(byff);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            os = openFileOutput("daneApp", Context.MODE_PRIVATE);
            os.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
