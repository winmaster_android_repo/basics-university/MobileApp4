package katarzyna.mobileapp4;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class AddDataActivity extends AppCompatActivity
{
    private FileOutputStream os;
    private FileInputStream is;

    private SharedPreferences pref;
    private View view;

    private TextView name;
    private TextView descr;
    private TextView amount;
    private TextView type;

    private RadioButton fruits;
    private RadioButton  vege;
    private RadioButton  milk;
    private RadioButton bread;

    private Button save;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);

        pref = getSharedPreferences("plik", Activity.MODE_PRIVATE);
        view=findViewById(R.id.activity_add_data);
        name=(TextView)findViewById(R.id.textViewName);
        descr=(TextView)findViewById(R.id.textViewDesc);
        amount=(TextView)findViewById(R.id.textViewAmount);
        type=(TextView)findViewById(R.id.textViewType);

        fruits=(RadioButton)findViewById(R.id.owoce);
        vege=(RadioButton)findViewById(R.id.warzywa);
        milk=(RadioButton)findViewById(R.id.nabial);
        bread=(RadioButton)findViewById(R.id.pieczywo);

        save=(Button)findViewById(R.id.buttonZapisz);

        getData();
    }

    protected void onRestart()
    {
        super.onRestart();
        getData();
    }

    public void save(View view)
    {
        byte[] byff = null;
        try
        {
            is = openFileInput("daneApp");
            byff = new byte[1024];
            is.read(byff);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            os = openFileOutput("daneApp", Context.MODE_PRIVATE);
            os.write(makeString().getBytes());
            os.write(byff);
            os.close();
            Toast.makeText(getApplicationContext(),"Zapisano!",Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String makeString()
    {
        String k = "none";

        if(milk.isChecked())
        {
            k="nabiał";
        }
        else
        {
            if(bread.isChecked())
            {
                k = "pieczywo";
            }
            else
            {
                if(fruits.isChecked())
                {
                    k="owoce";
                }
                else
                {
                    if(vege.isChecked())
                    {
                        k="warzywa";
                    }
                }
            }
        }


        EditText name = (EditText)findViewById(R.id.editTextName);
        EditText amount = (EditText)findViewById(R.id.editTextAmount);
        EditText desc = (EditText)findViewById(R.id.editTextDesc);

        String nameText;
        if(name.getText().toString().equals(""))
        {
            nameText="none";
        }
        else
        {
            nameText=name.getText().toString();
        }
        String amountText;
        if(amount.getText().toString().equals(""))
        {
            amountText="none";
        }
        else
        {
            amountText=amount.getText().toString();
        }
        String descText;
        if(desc.getText().toString().equals(""))
        {
            descText="none";
        }
        else
        {
            descText=desc.getText().toString();
        }

        return nameText+";"+k+";"+amountText+";"+descText+"#";
    }

    private void getData()
    {
        view.setBackgroundColor(pref.getInt("tlo", Color.DKGRAY));
        name.setTextColor(pref.getInt("tColor",Color.BLACK));
        name.setTextSize(pref.getInt("wielkosc",25));
        descr.setTextColor(pref.getInt("tColor",Color.BLACK));
        descr.setTextSize(pref.getInt("wielkosc",25));
        amount.setTextColor(pref.getInt("tColor",Color.BLACK));
        amount.setTextSize(pref.getInt("wielkosc",25));
        type.setTextColor(pref.getInt("tColor",Color.BLACK));
        type.setTextSize(pref.getInt("wielkosc",25));

        fruits.setTextColor(pref.getInt("tColor",Color.BLACK));
        vege.setTextColor(pref.getInt("tColor",Color.BLACK));
        bread.setTextColor(pref.getInt("tColor",Color.BLACK));
        milk.setTextColor(pref.getInt("tColor",Color.BLACK));
        save.setTextSize(pref.getInt("wielkosc",25));;
        save.setTextColor(pref.getInt("tColor",Color.BLACK));
    }
}
