package katarzyna.mobileapp4;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;

public class ListActivity extends AppCompatActivity
{
    private String[] lista1;
    private String[] lista2;
    private  String[] lista3;
    FileInputStream is;

    private SharedPreferences pref;
    private View view;
    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        fillList1();
        setContentView(R.layout.activity_list);

        myAdapter adapter = new myAdapter();
        ListView lista3 = (ListView) findViewById(R.id.lista);
        lista3.setAdapter(adapter);
        pref = getSharedPreferences("plik", Activity.MODE_PRIVATE);
        view=findViewById(R.id.activity_list);

        getData();
    }

    protected void onRestart()
    {
        super.onRestart();
        getData();
    }

    private void fillList1()
    {
        String file = null;
        try
        {
            is = openFileInput("daneApp");
            byte[] byff = new byte[1024];
            is.read(byff);
            file = new String(byff,"UTF-8");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if(file!=null)
        {
            lista1 = file.split("#");
            if(lista1.length>0)
            {
                lista2 = new String[lista1.length-1];
                lista3 = new String[lista1.length-1];
                for(int i =0;i<lista2.length;i++)
                {
                    lista2[i] = lista1[i].split(";")[0];
                    lista3[i] = lista1[i].split(";")[1];
                }
            }
        }
    }

    public class myAdapter extends BaseAdapter
    {
        private LayoutInflater inflater = null;
        public ImageView img;
        public int getCount()
        {
            return lista2.length;
        }

        public Object getItem(int position)
        {
            return lista1[position];
        }

        public long getItemId(int position)
        {
            return position;
        }

        public View getView(final int poss, View cView, final ViewGroup parent)
        {
            View mV;
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(cView == null) {
                cView = inflater.inflate(R.layout.listowy, null);
            }
            mV = cView;
            img = (ImageView) mV.findViewById(R.id.row_image);

            if(lista3[poss].equals("nabiał"))
            {
                System.out.println(lista3[poss]);
                img.setImageResource(R.drawable.img3);
            }
            else
            {
                if(lista3[poss].equals("pieczywo"))
                {
                    img.setImageResource(R.drawable.img4);
                }
                else
                {
                    if(lista3[poss].equals("owoce"))
                    {
                        img.setImageResource(R.drawable.img2);
                    }
                    else
                    {
                        img.setImageResource(R.drawable.img1);
                    }
                }
            }
            tv1 = (TextView) mV.findViewById(R.id.row_tv1);
            tv1.setText(lista2[poss]);
            TextView tv2 = (TextView) mV.findViewById(R.id.row_tv2);
            tv2.setText(lista3[poss]);

            tv1.setTextColor(pref.getInt("tColor",Color.BLACK));
            tv1.setTextSize(pref.getInt("wielkosc",25));

            mV.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    String clickedObj = (String)getItem(poss);
                    moreInfo(poss);
                }
            });

            return mV;
        }

    }

    public void moreInfo(int id)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] dane = unpackString(lista1[id]);
        builder.setTitle(dane[0]);
        builder.setMessage(String.format("RODZAJ: %s \nSzt: %s \nOpis: %s ", dane[1], dane[2], dane[3]));
        builder.create();
        builder.show();
    }

    public String[] unpackString(String data)
    {
        String unpacked[] = data.split(";");
        return unpacked;
    }

    private void getData()
    {
        view.setBackgroundColor(pref.getInt("tlo", Color.DKGRAY));
    }
}

